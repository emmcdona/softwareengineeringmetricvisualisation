import sys
paths = sys.path
paths.append('C:\Program Files (x86)\Python36-32\Lib\site-packages')
paths.append('C:\Program Files (x86)\Python36-32\Lib')
paths.append('C:\Program Files (x86)\Python36-32\DLLs')
paths.append('C:/Users/Emmet/AppData/Roaming/Python/Python36/site-packages')
# ^A bunch of awkward nonsense because PYTHONPATH is broken for some reason

from atlassian.bitbucket import Cloud 	# bitbucket api access
import json				# for converting and storing our dict as a string/json file
import requests				# for gathering api data via href links
import os
from faker import Faker			# for anonymisation
from collections import defaultdict

faker = Faker()
names = defaultdict(faker.name)

usr = os.getenv('BBUCKET_USR')
print ("Username: " + usr)

# Creating Cloud object to gather repo data from bitbucket API
bBucket = Cloud(
	username = usr,
	cloud = True)

repos = bBucket.repositories.get(bBucket.username)

num = repos['size']

relVals = [None] * num
values = repos['values']
relVals = [None] * num

# Populating relevant data from 'values' in the API json (name of repo, links to commits of repo, 
# repo creation date, owner of repo, and repo description.
for i in range(num):
	relVals[i] =	{
			'name': values[i]['name'],
			'commits': values[i]['links']['commits']['href'],
			'created_on': values[i]['created_on'],
			'owner_name': names[values[i]['owner']['display_name']], #anonymised
			'desc': values[i]['description']
			}	

# Populating relevant higher-level data for uor 'relRepo' json (main repo user, number of repos, sublist of values).
relRepo = {
	   'user': bBucket.username,
	   'num': num,
	   'values': relVals
	  }

print (str(num) + " repositories detected") #<- Mark of progress in application running

# Using 'requests' library, gather everything in the commits links from relVals, putting the relevant
# data into relCmts (Commit hash, commit author, date of commit and attached message)
commits = [None] * num
relCmts = [None] * num
for i in range(num):
	relCmts[i] = {'values': []}
	commits[i] = (requests.get(relVals[i]['commits'])).json()
	values = commits[i]['values']
	for value in values:
		x =	{
			'hash': value['hash'],
			'author': names[value['author']['raw']], #anonymised
			'date': value['date'],
			'msg': value['summary']['raw']
			}
		relCmts[i]['values'].append(x)

# replace links in relVals['commits'] with the gathered data from relCmts
for i in range(num):
	relVals[i]['commits'] = relCmts[i]

#print ("Repos: " + json.dumps(str(relRepo)))

with open('repos.json', 'w') as outfile:
	json.dump(relRepo, outfile)
print ("repos.json file created!") #<- final print to signify script.py completion

# SoftwareEngineeringMetricVisualisation

SoftwareEngineeringMetricVisualisation is a Python/HTML/Javascript project that attempts to visualise the 'Software Engineering' of a Bitbucket User.
This visualisation is achieved by displaying a timeline of commits that have been pushed to the repo, as commits signify change , and change signifies progress.
As well as the timeline, infoboxes on each commit are displayed, including links back to Bitbucket if the user wants a closer look.
All identifying information, but for the username being examined, is anonymised via Faker.

## Installation

To install, simply clone the repository, and run the install.sh shell file.

```bash
./install.sh
```

## Usage
Before using this application, make sure to have the environment variable BBUCKET_USR set to the username of the user you wish to visualize the Software Engineering of.

Once you have a localhost server running, via the run_server.sh shell file, call:

```bash
./run.sh
```

Alternatively, simply call
```bash
python3 script.py
```

Once either of the above commands are called, localhost:8000 should be displaying the visualisation.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Report
As the aim of this project was to visualize a metric for the measurement of software engineering, I first had to decide for myself what sort of metric I could visualize.

In looking at the sort of data available to me via the Bitbucket API, I found that there was, unfortunately, much less data available than one could find on Github, which is why my MeasuringSoftwareEngineeringReport focuses more on the Github API. With these limitations, I found myself working with the metric 'number / proximity of commits'.

![full](/images/full.PNG)

These commits are visualised as points on a timeline from the repository's creation to the time of the most recent commit. The points are transparent and assigned random colours in order to help distinguish between multiple commits in close proximity

![timeline](/images/timeline.PNG)

For a bit of aesthetic flair, these timelines' visibility can be toggled by pressing the button with general eatils about the repository.

![toggle1](/images/toggle1.PNG)

![toggle2](/images/toggle2.PNG)

As well as the timeline, this toggleable section includes descriptions of each commit, so a user can get a better idea of the progress achieved in each commit.

![paragraphs](/images/paragraphs.PNG)

For anonymisation purposes, the Faker Python library was used to provide fake names for everyone who commits to a given repo.

![names](/images/names.PNG)

## License
[MIT](https://choosealicense.com/licenses/mit/)